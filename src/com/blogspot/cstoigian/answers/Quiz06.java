package com.blogspot.cstoigian.answers;

import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Scanner;

public class Quiz06 {

	private static Scanner scan;

	//
	public static void main(String[] args) {

		// Nhap so phan tu trong mang
		scan = new Scanner(System.in);
		int n = 0;
		
		// Chi nhap so
		boolean check = false;
		do {
			try {
				System.out.print("Input n = ");
				n = scan.nextInt();
				check = true;
			} catch (InputMismatchException imex) {
				System.out.println("Number only!!!");
				scan.nextLine();
			}
		} while (!check);

		// Khai bao mang a
		float a[] = new float[n];

		// Nhap gia tri cho cac phan tu trong mang a
		for (int i = 0; i < n; i++) {
			System.out.print("Input a[" + i + "] = ");
			a[i] = scan.nextFloat();
		}

		// Dem so lan xuat hien cua moi phan tu trong mang a
		Map<Float, Integer> countHashMap = new HashMap<>();
		for (int i = 0; i < n; i++) {
			float key = a[i];
			if (!countHashMap.containsKey(key)) {
				countHashMap.put(key, 1);
			} else {
				int count = countHashMap.get(key) + 1;
				countHashMap.put(key, count);
			}
		}

		// Thong ke so lan xuat hien cua tat ca cac phan tu trong mang a
		System.out.println();
		System.out.println("Number of occurences for each array element: ");
		for (Map.Entry<Float, Integer> entry : countHashMap.entrySet()) {
			System.out.print(entry.getKey() + " [" + entry.getValue() + "] ");
		}

		// In ra nhung phan tu chi xuat hien mot lan
		System.out.println();
		System.out.println("Array elements that appear only once: ");
		boolean flag = false;
		for (Map.Entry<Float, Integer> entry : countHashMap.entrySet()) {
			if (entry.getValue() == 1) {
				flag = true;
				System.out.print(entry.getKey() + " ");
			}
		}
		if (!flag) {
			System.out.println("No element!!!");
		}

		// In ra nhung phan tu chi xuat hien hai lan
		System.out.println();
		System.out.println("Array elements that appear only twice: ");
		flag = false;
		for (Map.Entry<Float, Integer> entry : countHashMap.entrySet()) {
			if (entry.getValue() == 2) {
				flag = true;
				System.out.print(entry.getKey() + " ");
			}
		}
		if (!flag) {
			System.out.println("No element!!!");
		}

	}
}
